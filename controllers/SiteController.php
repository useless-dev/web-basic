<?php

namespace app\controllers;

use Yii;

use app\models\LoginForm;
use app\models\ContactForm;
use linslin\yii2\curl;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;

class SiteController extends MainController
{
    
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionDataAdmin()
    {
        $curl = new curl\Curl();
        $data = new ArrayDataProvider();
        $response = $curl->setGetParams([
            'access-token' => Yii::$app->params['token']
        ])->get($this->urlApi('admins'));
        if ($curl->errorCode === null) {
            $response = json_decode($response);
            $data = new ArrayDataProvider(['allModels' => $response->items]);
        }
        else {
            Yii::$app->session->setFlash('error', $response->status.': '.$response->message);
        }
        return $this->render('data-dosen', ['data' => $data]);
    }

    public function actionTambahAdmin()
    {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $curl = new curl\Curl();
            $postData = [
                'username' => $post['username'],
                'password' => $post['password'],
                'session' => $post['session'],
                'level' => $post['level'],
                'nama' => $post['nama'],
            ];
            $response = $curl->setPostParams($postData)->setGetParams([
                'access-token' => Yii::$app->params['token'],
            ])->post($this->urlApi('admins'));
            if ($curl->errorCode === null) {
                if ($curl->responseCode != 200 && $curl->responseCode != 201) {
                    Yii::$app->session->setFlash('error', $response);
                }
                else {
                    return $this->redirect(['data-admin']);
                }
            }
            else {
                Yii::$app->session->setFlash('error', $response->status.': '.$response->message);
            }
        }
        return $this->render('create-admin', [
            'data' => [
                'username' => '',
                'password' => '',
                'level' => '',
                'nama' => '',
                'sessions' => '',
            ]
        ]);
    }

    public function actionUpdate($id)
    {
        $curl = new curl\Curl();
        if (Yii::$app->request->isPost) {
            $curlUpdate = new curl\Curl();
            $post = Yii::$app->request->post();
            $postData = [
                'username' => $post['username'],
                'password' => $post['password'],
                'session' => $post['session'],
                'level' => $post['level'],
                'nama' => $post['nama'],
            ];
            $response = $curl->setPostParams($postData)->setGetParams([
                'access-token' => Yii::$app->params['token'],
                'id' => $id
            ])->put($this->urlApi('admin/update'));
            if ($curl->errorCode === null) {
                if ($curl->responseCode != 200) {
                    Yii::$app->session->setFlash('error', $response);
                }
                else {
                    return $this->redirect(['data-admin']);
                }
            }
            else {
                Yii::$app->session->setFlash('error', $response->status.': '.$response->message);
            }
        }
        $curl = new curl\Curl();
        $response = $curl->setGetParams([
            'access-token' => Yii::$app->params['token'],
        ])->get($this->urlApi('admins/'.$id));
        if ($curl->errorCode === null) {
            if ($curl->responseCode != 200) {
                throw new NotFoundHttpException('Halaman tidak ditemukan');
            }
            else {
                $response = json_decode($response);
            }
        }
        return $this->render('update', ['data' => $response, 'id' => $id]);
    }

    public function actionDelete($id)
    {
        $curl = new curl\Curl();
        $response = $curl->setGetParams([
            'access-token' => Yii::$app->params['token'],
        ])->delete($this->urlApi('admins/'.$id));
        if ($curl->errorCode === null) {
            if ($curl->responseCode != 204) {
                throw new NotFoundHttpException('Halaman tidak ditemukan');
            }
        }
        return $this->redirect(['data-admin']);
    }   
}


 // public function actionLogin()
 //    {
 //        if (!Yii::$app->user->isGuest) {
 //            return $this->goHome();
 //        }

 //        $model = new LoginForm();
 //        if ($model->load(Yii::$app->request->post()) && $model->login()) {
 //            return $this->goBack();
 //        }

 //        $model->password = '';
 //        return $this->render('login', [
 //            'model' => $model,
 //        ]);
 //    }

 //    public function actionLogout()
 //    {
 //        Yii::$app->user->logout();

 //        return $this->goHome();
 //    }

 //    public function actionAbout()
 //    {
 //        return $this->render('about');
 //    }

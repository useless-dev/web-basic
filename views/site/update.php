<?php 
	use yii\helpers\Url;
	use yii\bootstrap4\ActiveForm;
	$this->title = 'Ubah Admin';
	$this->params['breadcrumbs'][] = ['url' => ['data-admin'], 'label' => 'Admin'];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<?php ActiveForm::begin(['action' => ['update', 'id' => $id]]) ?>
					<div class="form-group">
						<label>Username</label>
						<input type="text" name="username" class="form-control" value="<?= $data->username ?>">
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="text" name="password" class="form-control" value="<?= $data->password ?>">
					</div>
					<div class="form-group">
						<label>Session</label>
						<input type="text" name="session" class="form-control" value="<?= $data->sessions ?>">
					</div>
					<div class="form-group">
						<label>Level</label>
						<input type="text" name="level" class="form-control" value="<?= $data->level ?>">
					</div>
					<div class="form-group">
						<label>Nama</label>
						<input type="text" name="nama" class="form-control" value="<?= $data->nama ?>">
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">
							Simpan data
						</button>
					</div>
				<?php ActiveForm::end() ?>
			</div>
		</div>
	</div>
</div>
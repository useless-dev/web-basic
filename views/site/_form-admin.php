<?php 
	use yii\helpers\Url;
	use yii\bootstrap4\ActiveForm;
	$data = json_decode($data['data']);
?>
<?php ActiveForm::begin(['action' => ['tambah-admin']]) ?>
	<div class="form-group">
		<label>Username</label>
		<input type="text" name="username" class="form-control">
	</div>
	<div class="form-group">
		<label>Password</label>
		<input type="text" name="password" class="form-control">
	</div>
	<div class="form-group">
		<label>Session</label>
		<input type="text" name="session" class="form-control">
	</div>
	<div class="form-group">
		<label>Level</label>
		<input type="text" name="level" class="form-control">
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-primary">
			Simpan data
		</button>
	</div>
<?php ActiveForm::end() ?>
<?php 
	use yii\grid\GridView;
	use yii\grid\SerialColumn;
	use yii\grid\ActionColumn;
	use yii\helpers\Url;
	use yii\helpers\Html;
?>

<div class="row">
	<div class="col-md-12">
		<?php if(Yii::$app->session->hasFlash('error')) : ?>
			<div class="alert alert-danger" role="alert">
				<?= Yii::$app->session->getFlash('error') ?>
			</div>
		<?php endif ?>
		<?php if(Yii::$app->session->hasFlash('success')) : ?>
			<div class="alert alert-success" role="alert">
				<?= Yii::$app->session->getFlash('success') ?>
			</div>
		<?php endif ?>
	</div>
	<div class="col-md-12">
		<a href="<?= Url::to(['tambah-admin']) ?>">
			Admin Baru
		</a>
		<div class="card">
			<div class="card-body">
				<?=
					GridView::widget([
						'dataProvider' => $data,
						'columns' => [
							['class' => SerialColumn::className()],
							'username',
							'password',
							'sessions',
							'level',
							'nama',
							[
								'class' => ActionColumn::className(), 
								'template' => '{update} {delete}',
							]
						]
					]);
				?>
			</div>
		</div>
	</div>
</div>
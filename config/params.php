<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'token' => 'MA7ZV-dnbT2iP3yVDjwfBRgY8-QWOvXg',
    'api' => 'http://localhost:8080'
];
